import * as React from 'react';
import { connect } from 'react-redux';

import { TodosComponent } from '../todos/todos.component';
import { TodosState } from '../../model/state';
import { createCompleteTodo } from '../../model/actions';

const mapStateToProps: any = (state: TodosState): TodosState => {
	console.log(state);
	return {
		todos: state.todos
	}
};

const mapDispatchToProps: any = (dispatch: any) => {
	return {
		onTodoClick: (id: number) => {
			dispatch(createCompleteTodo(id))
		}
	}
};

const VisibleTodosComponent: any = connect(
	mapStateToProps,
	mapDispatchToProps
)(TodosComponent);

export default VisibleTodosComponent
