import * as React from 'react';

import VisibleTodosComponent from '../visible-todos/visible-todos.component';
import AddTodoComponent from '../add-todo/add-todo.component';

export class AppComponent extends React.Component<void, void> {
	render(): JSX.Element {
		return <div>
			<AddTodoComponent />
			<VisibleTodosComponent />
		</div>;
	}
}
