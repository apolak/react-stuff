import * as React from "React";
import { Todo, TodoComponent } from '../todo/todo.component';

type TodosProps = {
	todos: Todo[],
	onTodoClick: Function
}

export class TodosComponent extends React.Component<TodosProps, void> {
	render(): JSX.Element {
		return <ul>
			{
				this.props.todos.map((todo: Todo):JSX.Element =>
					<TodoComponent key={todo.id} {...todo} onClick={() => this.props.onTodoClick(todo.id)}/>
				)
			}
		</ul>
	}
}
