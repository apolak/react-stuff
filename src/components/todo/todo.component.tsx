import * as React from "react";

type TodoProps = {
	text: string,
	completed: boolean,
	onClick: React.EventHandler<React.SyntheticEvent<HTMLElement>>
}

export type Todo = {
	text: string,
	completed: boolean,
	id: number
};

export class TodoComponent extends React.Component<TodoProps, void> {
	render(): JSX.Element {
		return (
			<li onClick={this.props.onClick} style={{textDecoration: this.props.completed ? 'line-through': 'none' }}>
				<a href="http://onet.pl" onClick={this.handleClick.bind(this)}>
					{this.props.text}
				</a>
			</li>
		);
	}

	private handleClick(event: React.SyntheticEvent<HTMLElement>): void {
		event.preventDefault();
		console.log("this is stupid, but is here just to test stuff");
	}
}
