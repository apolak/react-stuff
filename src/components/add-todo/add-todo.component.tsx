import * as React from 'react';
import { connect } from 'react-redux';
import { createAddTodo } from '../../model/actions';

type AddTodoProps = {
	onSubmit: Function
}

class AddTodo extends React.Component<AddTodoProps, void> {
	input: HTMLInputElement;

	render(): JSX.Element {
		return(
			<div>
				<form onSubmit={event => {
					event.preventDefault();

					if(!this.input.value.trim()) {
						return;
					}

					this.props.onSubmit(this.input.value);

					this.input.value = '';
				}}>
					<input ref={node => {
						this.input = node;
					}}/>
					<button type="submit">Add todo</button>
				</form>
			</div>
		)
	}
}

const mapDispatchToProps: any = (dispatch: any) => {
	return {
		onSubmit: (textValue: string) => {
			dispatch(createAddTodo(textValue))
		}
	}
};

const AddTodoComponent = connect(null, mapDispatchToProps)(AddTodo);

export default AddTodoComponent;
