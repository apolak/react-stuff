import * as React from 'react';
import * as ReactDom from 'react-dom';
import { AppComponent } from './components/app/app.component';
import { createStore } from 'redux';
import { Provider} from 'react-redux';
import { reducer, initialState } from './model/reducer';

const store: any = createStore(reducer, initialState);

ReactDom.render(
	<Provider store={store}>
		<AppComponent/>
	</Provider>,
	document.getElementById('example')
);
