import { Action } from 'redux';
export const ADD_TODO: string = 'ADD_TODO';
export const COMPLETE_TODO: string = 'COMPLETE_TODO';

export interface TodoAction extends Action {
	id: number
}

export interface CreateTodoAction extends Action {
	text: string
}

export const createAddTodo:Function = (text:string): CreateTodoAction => {
	return {
		type: ADD_TODO,
		text: text
	}
};

export const createCompleteTodo:Function = (id:number): TodoAction => {
	return {
		type: COMPLETE_TODO,
		id: id
	}
};
