import { Todo } from '../components/todo/todo.component';

export type TodosState = {
	todos: Todo[]
};
