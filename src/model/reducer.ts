import { TodosState } from './state';
import { COMPLETE_TODO, ADD_TODO } from './actions';
import { Reducer } from 'redux';
import * as _ from 'lodash';
import { Todo } from '../components/todo/todo.component';

export const initialState: TodosState = (localStorage.getItem('test-todo-list') === null) ? {
	todos: []
} : JSON.parse(localStorage.getItem('test-todo-list'));

export const reducer: Reducer<any> =  (state:TodosState = initialState, action: any): TodosState => {
	let newState: TodosState = null;

	switch (action.type) {
		case ADD_TODO:
			let id: number = 1;

			if (state.todos.length > 0) {
				id = state.todos[state.todos.length-1].id +1;
			}

			// This seems strange, but in order to redux notice a change we can't use old state. We must create
			// a completly new one .. saddly we have no Object.assign yet so I use Lodash _.cloneDeep
			let todos: Todo[] = _.cloneDeep(state.todos);

			todos.push({
				id: id,
				completed: false,
				text: action.text
			});

			newState =  {
				todos: todos
			};

			localStorage.setItem('test-todo-list', JSON.stringify(newState));

			return newState;
		case COMPLETE_TODO:
			newState = _.cloneDeep(state);

			for(let i=0; i<newState.todos.length; i++) {
				if (newState.todos[i].id === action.id) {
					newState.todos[i].completed = true;
				}
			}

			localStorage.setItem('test-todo-list', JSON.stringify(newState));

			return newState;
		default:
			return state;
	}
};
